using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


public class ItemsGenerator : MonoBehaviour
{
    [SerializeField] private List<GameObject> extraItemPrefabList;
    [SerializeField] private List<GameObject> gemItemPrefabList;


    private List<GameObject> _randomItemList;

    private void ExpandRandListWithBlocks(int count=2)
    {
        for (var i = 0; i < count; i++)
        {   
            _randomItemList.AddRange(extraItemPrefabList);
        }
    }

    private void Awake()
    {
        _randomItemList = new List<GameObject>();
        _randomItemList.AddRange(gemItemPrefabList);
        ExpandRandListWithBlocks(1);
    }

    
    
    private void Update()
    {
        if (Board.IsFull()) return;

        for (var x = 0; x < Board.instance.widthCount; x++)
        {
            if(Board.GetItem(x, Board.instance.heightCount - 1)) continue;
            if(Board.GetItem(x, Board.instance.heightCount)) continue;
            CreateItem(x);
        }    
    }

    private void CreateItem(int x)
    {
        var index = Random.Range(0, _randomItemList.Count);
        var prefab = _randomItemList[index];
        _randomItemList.RemoveAt(index);

        foreach (var gemItem in gemItemPrefabList.Where(gemItem => prefab != gemItem))
        {
            _randomItemList.Add(gemItem);
        }
        foreach (var gemItem in extraItemPrefabList.Where(gemItem => prefab != gemItem))
        {
            _randomItemList.Add(gemItem);
        }

        var newItem = Instantiate(prefab, transform).GetComponent<Item>();
        newItem.typeName = prefab.name;
        newItem.ForceMove(x, Board.instance.heightCount);
    }
}
