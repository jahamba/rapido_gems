using System.Collections.Generic;

public class Index
{
    public Index(int nx, int ny)
    {
        X = nx;
        Y = ny;
    }
    
    public int X { get; }
    public int Y { get; }
}

public static class NeighbourIndexes
{
    public static List<Index> List { get; } = new List<Index>
    {
        new Index(1, 0),
        new Index(-1, 0),
        new Index(0, 1),
        new Index(0, -1)
    };
}
