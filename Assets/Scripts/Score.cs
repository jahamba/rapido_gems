using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    private static Score _instance;

    private float _visibleValue;
    private int _value;

    private const float ChangeAmount = 10;

    public static int Value => _instance._value;

    private TextMeshProUGUI _textMeshProUGUI;
    
    private Score()
    {
        _instance = this;
    }

    private void Awake()
    {
        _value = 0;
        _visibleValue = 0;
        _textMeshProUGUI = GetComponent<TextMeshProUGUI>();
    }


    public static void AddScore(int addValue)
    {
        _instance._value += addValue;
    }

    private void Update()
    {
        if (Math.Abs(_visibleValue - _value) < 0.05f) return;

        _textMeshProUGUI.text = Mathf.RoundToInt(_visibleValue).ToString();
        _visibleValue += ChangeAmount * Time.deltaTime;
        if (_visibleValue < _value) return;
        _visibleValue = _value;
    }
}
