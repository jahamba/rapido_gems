using System;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public static Board instance;

    #region PARAMETERS OF PREFAB
    
    [SerializeField] private RectTransform topFrame;
    public int widthCount = 5;
    public int heightCount = 9;
    
    #endregion
    
    #region INIT METHODS
    
    private Board()
    {
        instance = this;
    }

    private void Awake()
    {
        _items = new Item[widthCount, heightCount + 1];
        InitializeBorderAndCellSideSize();
    }
    
    private void InitializeBorderAndCellSideSize()
    {
        var rect = GetComponent<RectTransform>().rect;
        _border = rect.width * GridBorders;
        _cellSideSize = (rect.width - _border * 2) / widthCount;
        topFrame.sizeDelta = new Vector2(rect.width, rect.height - _border - _cellSideSize * heightCount);
    }
    
    #endregion

    #region POSITIONS

    private static float _border;
    private static float _cellSideSize;
    private const float GridBorders = 0.05f;
    public static Vector2 CellSize => new Vector2(_cellSideSize, _cellSideSize);
    
    public static Vector2 GetPositionOfPoint(int x, int y)
    {
        return new Vector2(GetPositionX(x), GetPositionY(y));
    }

    private static float GetPositionX(int x)
    {
        return _border + (x + .5f) * _cellSideSize;
    }
    public static float GetPositionY(int y)
    {
        return _border + (y + .5f) * _cellSideSize;
    }

    #endregion

    #region ITEMS
    
    private Item[,] _items;
    private int _itemsCount = 0;

    private static bool CheckXY(int x, int y)
    {
        return !(x < 0 || x >= instance.widthCount || y < 0 || y >= instance.heightCount + 1);
    }
    
    public static Item GetItem(int x, int y)
    {
        return !CheckXY(x, y) ? null : instance._items[x, y];
    }

    public static bool IsNeighbours(Item item, int x, int y)
    {
        var neighbour = GetItem(x, y);
        if (neighbour == null) return false;
        return item.Chain == neighbour.Chain;
    }
    
    
    
    public static void SetItem(Item item, int x, int y)
    {
        if (!CheckXY(x, y)) return;
        
        if (instance._items[x, y] == null && item != null)
            instance._itemsCount += 1;
        
        if (instance._items[x, y] != null && item == null)
            instance._itemsCount -= 1;

        
        instance._items[x, y] = item;
        
    }
    
    public static void ClearItem(int x, int y)
    {
        if (!CheckXY(x, y)) return;

        if (instance._items[x, y] != null)
            instance._itemsCount -= 1;
        
        instance._items[x, y] = null;
    }
    
    public static bool IsFull()
    {
        return instance._itemsCount >= (instance.widthCount * instance.heightCount);
    }
    
    #endregion
   
}
