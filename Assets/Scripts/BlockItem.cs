using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockItem : Item
{
    [SerializeField] private List<Texture> textures;
    [SerializeField] private RawImage iconRawImage;
    
    private int _health = 3;

    public override void Kick()
    {
        
        //StartBlinking();
        //BlinkingAction += KickHappened;
        
        KickHappened();
    }

    private void KickHappened()
    {
        //BlinkingAction -= KickHappened;
        _itemState = ItemState.Staying;
        _health -= 1;
        if (_health <= 0)
        {
            Kill();
            return;
        }
        iconRawImage.texture = textures[_health - 1];
    }

    protected override void Blink(bool value)
    {
        //iconRawImage.gameObject.SetActive(value);
        blink = value;
    }
}
