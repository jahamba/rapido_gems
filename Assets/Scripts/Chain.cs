using System.Collections.Generic;
using System.Linq;


public class Chain
{
    private readonly List<Item> _items = new List<Item>();

    public void AddItem(Item item)
    {
        if (_items.Contains(item)) return;
        _items.Add(item);
    }

    public void MergeWithChain(Chain target)
    {
        if(target == this) return;
        foreach (var item in target._items)
        {
            item.Chain = this;
            AddItem(item);
        }
    }

    public void RemoveItem(Item item)
    {
        
        item.Chain = null;
        item.itemBackground.Redraw();
        var oldItems = _items.Where(oldItem => oldItem != item).ToList();

        foreach (var oldItem in oldItems)
        {
            oldItem.Chain = null;
        }

        foreach (var oldItem in oldItems)
        {
            oldItem.FindChain();
        }
        
        _items.Clear();
        
    }

    public void RedrawChain()
    {
        foreach (var item in _items)
        {
            item.itemBackground.Redraw();
        }
    }

    private int GetScore(int count)
    {
        return count switch
        {
            0 => 0,
            1 => 0,
            2 => 1,
            3 => 2,
            4 => 4,
            5 => 7,
            6 => 11,
            7 => 16,
            8 => 22,
            9 => 29,
            _ => 37
        };
    }
    
    public void Explode()
    {
        Score.AddScore(GetScore(_items.Count));
        
        foreach (var item in _items)
        {
            foreach (var index in NeighbourIndexes.List)
            {
                var neighbour = Board.GetItem(item.X + index.X, item.Y + index.Y);
                if (neighbour == null) continue;
                if (neighbour.IsEqualType(item)) continue;
                neighbour.Kick();
            }
        }
        
        foreach (var item in _items)
        {
            item.Explode();
        }
    }
}
