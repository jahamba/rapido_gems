using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;

public class ItemBackground : MonoBehaviour
{
    [SerializeField] private List<RawImage> linksRawImages;
    [SerializeField] private ProceduralImage backgroundProceduralImage;

    private Item _item;
    private Color _color;
    private readonly Color _defaultColor = new Color(0.85f, 0.85f, 0.85f);
    private void Awake()
    {
        _item = GetComponentInParent<Item>();
        AssignColor();
    }

    public void SetColor(Color value)
    {
        _color = value;
    }

    private void AssignColor(bool isDefault = true)
    {
        var col = isDefault ? _defaultColor : _color;
        foreach (var rawImage in linksRawImages)
        {
            rawImage.color = col;
        }

        backgroundProceduralImage.color = col;
    }

    public void Redraw()
    {
        if (_item == null) return;

        AssignColor(_item.Chain == null);

        if (_item.Chain == null)
        {
            foreach (var rawImage in linksRawImages)
            {
                rawImage.gameObject.SetActive(false);
            }
            return;
        }

        for (var i = 0; i < 4; i++)
        {
            linksRawImages[i].gameObject.SetActive(Board.IsNeighbours(
                _item,
                _item.X + NeighbourIndexes.List[i].X,
                _item.Y + NeighbourIndexes.List[i].Y));
        }
    }
}
