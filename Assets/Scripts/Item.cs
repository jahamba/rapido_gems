using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IPointerClickHandler
{
    #region PARAMETERS

    public Color colorForBackground;
    public ItemBackground itemBackground;

    private const float FallSpeed = 2000;
    protected bool linkable = false;
    
    public string typeName;
    
    #endregion

    #region INIT
    private void Awake()
    {
        itemBackground.SetColor(colorForBackground);
        itemBackground.gameObject.SetActive(linkable);
        
        GetComponent<RectTransform>().sizeDelta = Board.CellSize;

    }

    private void Start()
    {
        itemBackground.Redraw();
    }

    #endregion
    
    #region BOARDING
    
    public int X { get; private set; }
    public int Y { get; private set; }
    
    private void SetY(int ny)
    {
        Board.ClearItem(X, Y);
        Y = ny;
        Board.SetItem(this, X, Y);
    }
    
    public void ForceMove(int nx, int ny)
    {
        X = nx;
        Y = ny;
        transform.position = Board.GetPositionOfPoint(nx, ny);
        Board.SetItem(this, nx, ny);

    }
    
    public bool IsEqualType(Item item)
    {
        return typeName == item.typeName;
    }
    
    #endregion
    
    #region CHAINING
    
    public Chain Chain { get; set; } = null;
    
    public void FindChain()
    {
        if(!linkable) return;
        if (_itemState != ItemState.Staying) return;
        
        foreach (var index in NeighbourIndexes.List)
        {
            var neighbour = Board.GetItem(X + index.X, Y + index.Y);
            if (neighbour == null) continue;
            if (neighbour._itemState != ItemState.Staying) continue;
            TryToLinkItem(neighbour);
        }
        
        Chain?.RedrawChain();
        itemBackground.Redraw();
    }
    
    private void TryToLinkItem(Item item)
    {
        if (!linkable) return;
        if (typeName != item.typeName) return;

       
        var chains = new List<Chain>();
        if (Chain != null) chains.Add(Chain);
        if (item.Chain != null) chains.Add(item.Chain);
        
        switch (chains.Count)
        {
            
            case 0:
                Chain = new Chain();
                item.Chain = Chain;
                break;
            case 1:
                Chain = chains[0];
                item.Chain = chains[0];
                break;
            case 2:
                chains[0].MergeWithChain(chains[1]);
                break;
        }
        
        Chain?.AddItem(this);
        Chain?.AddItem(item);
    }
    
    
    
    #endregion

    #region ITEM STATE

    protected enum ItemState
    {
        Falling,
        Staying,
        Blinking
    }
    
    protected ItemState _itemState = ItemState.Falling;

    #endregion

    #region BLINKING

    protected event Action BlinkingAction;

    protected static readonly List<Item> blinkingItems = new List<Item>();

    protected bool SomeoneBlinking()
    {
        return blinkingItems.Count > 0;
    }
    
    protected void StartBlinking()
    {
        _itemState = ItemState.Blinking;
        _currentBlinkingTime = BlinkingTime;
    }

    protected virtual void Blink(bool value)
    {
        itemBackground.gameObject.SetActive(value);
        blink = value;
    }
    
    #endregion
    
    #region UPDATE

    private const float BlinkingTime = 0.3f;
    private float _currentBlinkingTime = 0;
    private const float BlinkPeriod = 0.1f;
    private float _currentBlink = 0;
    protected bool blink = false;
    
    private void Update()
    {
        FallUpdate();
        StayUpdate();
        ChainUpdate();
        BlinkUpdate();
    }

    

    private void FallUpdate()
    {
        if(SomeoneBlinking()) return;
        if (_itemState != ItemState.Falling) return;

        var t = transform;
        
        t.position += Vector3.down * (Time.deltaTime * FallSpeed);
        
        var targetY = Board.GetPositionY(Y);
        
        if (t.position.y > targetY) return;
        
        _itemState = ItemState.Staying;
        RemoveFallingItem(this);
        
        var position = t.position;
        position = new Vector3(position.x, targetY, position.z);
        t.position = position;
    }
    
    private void StayUpdate()
    {
        if (_itemState != ItemState.Staying) return;
        if (Y == 0) return;

        if (Board.GetItem(X, Y - 1) != null) return;
        SetY(Y - 1);
        _itemState = ItemState.Falling;
        AddFallingItem(this);
    }
    
    private void ChainUpdate()
    {

        switch (_itemState)
        {
            case ItemState.Falling:
                if (Chain == null) return;
                Chain.RemoveItem(this);
                break;
            case ItemState.Staying:
                FindChain();
                break;
        }
    }
    
    private void BlinkUpdate()
    {
        if (_itemState != ItemState.Blinking) return;
        
        _currentBlinkingTime -= Time.deltaTime;
        if(_currentBlinkingTime<0)
        {
            Blink(true);
            BlinkingAction?.Invoke();
            return;
        }

        _currentBlink -= Time.deltaTime;
        if (_currentBlink > 0) return;

        Blink(!blink);
        _currentBlink = BlinkPeriod;
    }

    #endregion

    #region INTERACTION

    public void OnPointerClick(PointerEventData eventData)
    {
        if (SomeoneFalling()) return;
        Chain?.Explode();
    }

    public void Explode()
    {
        StartBlinking();
        blinkingItems.Add(this);
        BlinkingAction += Kill;
    }

    protected void Kill()
    {
        blinkingItems.Remove(this);
        BlinkingAction -= Kill;
        Board.ClearItem(X, Y); 
        Destroy(gameObject);
    }
    
    public virtual void Kick() { }
    
    #endregion
   
    
    #region FALLING ITEMS
    
    private static readonly List<Item> FallingItems = new List<Item>();
    
    private static void AddFallingItem(Item item)
    {
        if(FallingItems.Contains(item)) return;
        FallingItems.Add(item);
    }

    private static void RemoveFallingItem(Item item)
    {
        if(!FallingItems.Contains(item)) return;
        FallingItems.Remove(item);
    }

    private static bool SomeoneFalling()
    {
        return FallingItems.Count > 0;
    }
    
    #endregion
    
}
